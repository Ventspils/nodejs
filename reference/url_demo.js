const url = require('url');

const myUrl = new URL(
  'http://mywebsite.com:5000/hello.html?id=100&status=active'
);

const name = 'Brad';

console.log(name);

// Serialized URL
console.log(myUrl.href);
console.log(myUrl.toString());

// Get host
console.log(myUrl.host);

// Get hostname
console.log(myUrl.hostname);

// Get pathname
console.log(myUrl.pathname);

// Serialized query
console.log(myUrl.search);

// Param object
console.log(myUrl.searchParams);

// Add param
myUrl.searchParams.append('abc', '123');
console.log(myUrl.searchParams);

// Loop through params
myUrl.searchParams.forEach((value, name) => {
  console.log(`${name}: ${value}`);
});

//Loop through params
console.log(myUrl.searchParams);

// Loop through params
console.log(myUrl.searchParams);

// Loop through params
console.log(myUrl.searchParams);

// Loop through params
console.log(myUrl.searchParams);

// Loop through params
console.log(myUrl.searchParams);

// Loop through params
console.log(myUrl.searchParams);

// Loop through params
console.log(myUrl.searchParams);

// Loop through params
console.log(myUrl.searchParams);

// Loop through params
console.log(myUrl.searchParams);
