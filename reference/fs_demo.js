const fs = require('fs');
const path = require('path');

// Cretate folder
// fs.mkdir(path.join(__dirname, 'test'), {}, (err) => {
//   if (err) throw err;
// });

// Write to a file
fs.writeFile(
  path.join(__dirname, 'test', 'hello.txt'),
  'Hello World!',
  (err) => {
    if (err) throw err;
    console.log('File is written...');
  }
);

// Read file
fs.readFile(path.join(__dirname, 'test', 'hello.txt'), 'utf8', (err, data) => {
  if (err) throw err;
  console.log(data);
});

// Rename file
fs.rename(
  path.join(__dirname, 'test', 'hello.txt'),
  path.join(__dirname, 'test', 'helloworld.txt'),
  (err) => {
    if (err) throw err;
  }
);
